#!/usr/bin/env python3
#
# The MIT License (MIT)
# Copyright (c) 2016 Jan Henrik Hasselberg
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

import json, argparse, csv, datetime
from urllib.request import urlopen
from os import path

REST_URI="https://www.hackerrank.com/rest/contests/master/hackers/{username}/profile"

def fetch_user_data(username):
    result_json = {}
    url=REST_URI.format(username=username)
    with urlopen(url) as data:
        result_json = json.loads(data.read().decode())
    return result_json

def get_ascii_leaderboard(normalized_data, team_name):
    result = "HackerRank Leaderboard"
    if team_name:
        result+=" for team {}".format(team_name)
    score_str = "#{pos:7} | {username:15} | {hacko_amount:12} | {languages}"
    score_items = []
    for i in normalized_data:
        score_item_str = score_str.format(\
                pos=str(i['position']),\
                username=i['username'],\
                hacko_amount=str(i['hacko_amount']),\
                languages=i['languages'])
        score_items.append(score_item_str)
    width = len(max(score_items, key=len))
    result+="\n"+'-'*width
    result+="\nPosition | Username        | Hacko Amount | Languages"
    result+="\n"+'-'*width
    for s in score_items:
        result+="\n"+s
    result+="\n"+'-'*width
    return result

def save_csv_leaderboard(normalized_data, team_name, csv_file):
    fieldnames = ['iso_datetime', 'username', 'position', 'hacko_amount', 'languages', 'team_name']
    iso_datetime = datetime.datetime.today().isoformat()
    file_mode='w'
    if path.exists(csv_file):
        file_mode='a'
    with open(csv_file, file_mode) as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        if file_mode=='w':
            writer.writeheader()
        for i in normalized_data:
            row = {'iso_datetime': iso_datetime, 'team_name': team_name}
            row.update(i)
            writer.writerow(row)
    return file_mode

def normalize_data(json_data):
    result = []
    sort_key=lambda i: int(i['model']['hacko_amount'])
    json_data_sorted = sorted(json_data, key=sort_key, reverse=True)
    for i in range(len(json_data_sorted)):
        model = json_data_sorted[i]["model"]
        user = {}
        user['position'] = i+1
        user['username'] = model["username"]
        user['languages'] = str([l[0] for l in model["languages"]])
        user['hacko_amount'] = model["hacko_amount"]
        result.append(user)
    return result

def get_arguments():
    parser = argparse.ArgumentParser(description='Generate a leaderboard from HackerRank from given usernames.')
    parser.add_argument('usernames', metavar='U', type=str, nargs='+', help='username from HackerRank')
    parser.add_argument('--team-name', metavar='N', type=str, help='team name', default='')
    parser.add_argument('--csv-file', metavar='F', type=str, help='write or append leaderboard to csv file', default='')
    args = parser.parse_args()
    usernames = args.usernames
    team_name = args.team_name
    csv_file = args.csv_file
    return (usernames, team_name, csv_file)

def main():
    usernames, team_name, csv_file = get_arguments()
    json_data = [fetch_user_data(u) for u in usernames]
    normalized = normalize_data(json_data)
    if not normalized:
        print("No data ...")
        exit()
    print(get_ascii_leaderboard(normalized, team_name))
    if csv_file:
        file_mode = save_csv_leaderboard(normalized, team_name, csv_file)
        if file_mode == 'w':
            print("Leaderboard data written to file: {}".format(csv_file))
        elif file_mode == 'a':
            print("Leaderboard data appended to file: {}".format(csv_file))

if __name__=="__main__":
    main()
