# hrleaderboard #

"hrleaderboard" is a [HackerRank](https://www.hackerrank.com) leaderboard generator written in Python 3. Just supply your team name and usernames and you will get a ranking based on "hacko amounts" printed to shell. Additionally, "hrleaderboard" supports storing leaderboard results into CSV file for later processing.

### Howto? ###

Make sure all team members has signed up and have a username on [HackerRank](https://www.hackerrank.com).

Use --help argument for guidance.

```
$ python3 hrleaderboard.py --help
usage: hrleaderboard.py [-h] [--team-name N] [--csv-file F] U [U ...]

Generate a leaderboard from HackerRank from given usernames.

positional arguments:
  U              username from HackerRank

optional arguments:
  -h, --help     show this help message and exit
  --team-name N  team name
  --csv-file F   write or append leaderboard to csv file

```

### Example ###

This example generates leaderboard with "[user1](https://www.hackerrank.com/user1)", "[user2](https://www.hackerrank.com/user2)" and "[user3](https://www.hackerrank.com/user3)". The team name is titled "Foo", but it can be anything.

```
$ python3 hrleaderboard.py user1 user2 user3 --team-name Foo
HackerRank Leaderboard for team Foo
-------------------------------------------------------------------------
Position | Username        | Hacko Amount | Languages
-------------------------------------------------------------------------
#1       | user2           | 135          | ['python3', 'python', 'bash']
#2       | user1           | 100          | ['c']
#3       | user3           | 100          | []
-------------------------------------------------------------------------
```
